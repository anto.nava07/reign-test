"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const fetch = require('node-fetch');
const dbConnection_1 = require("./dbConnection");
const index_1 = __importDefault(require("./routes/index"));
class Server {
    constructor() {
        this.app = (0, express_1.default)();
        this.config();
        this.routes();
    }
    config() {
        //Settings
        this.app.set('port', process.env.PORT || 8000);
        //Middleware
        this.app.use((0, morgan_1.default)('dev'));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use((0, helmet_1.default)());
        this.app.use((0, compression_1.default)()); //reduce el peso de las respuestas
        this.app.use((0, cors_1.default)());
    }
    routes() {
        this.app.use(index_1.default);
    }
    start() {
        this.app.listen(this.app.get('port')), () => {
            console.log('Server on port ', this.app.get('port'));
        };
        //Se obtienen los datos de la API al empezar y se guardan en la BDD
        obtenerDatos();
        //Se crea un timeout para volver a obtener los datos luego de una hora
        setTimeout(obtenerDatos, 3600000);
    }
}
//Fin clase Server
const obtenerDatos = () => __awaiter(void 0, void 0, void 0, function* () {
    const data = yield fetch("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");
    const body = yield data.json();
    const hits = body.hits;
    //Ordena por fecha, de antiguo a reciente para poder guardarse en BDD
    hits.sort((a, b) => {
        if (a.created_at > b.created_at) {
            return 1;
        }
        if (a.created_at < b.created_at) {
            return -1;
        }
        return 0;
    });
    guardarEnBDD(hits);
});
const guardarEnBDD = (hits) => __awaiter(void 0, void 0, void 0, function* () {
    //Se guarda cada item de los hits
    hits.map((item) => __awaiter(void 0, void 0, void 0, function* () {
        //console.log("Conseguir valor de created_at a partir de un item: ", item.created_at);
        yield dbConnection_1.pool.query('INSERT INTO news (info) VALUES ($1)', [item]);
    }));
});
const server = new Server();
server.start();
