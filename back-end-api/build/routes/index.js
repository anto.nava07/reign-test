"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = (0, express_1.Router)();
const index_controller_1 = require("../controllers/index.controller");
router.get('/news', index_controller_1.getNews);
router.delete('/news/:id', index_controller_1.deleteNews);
exports.default = router;
