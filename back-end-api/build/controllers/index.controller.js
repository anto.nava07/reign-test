"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteNews = exports.getNews = void 0;
const dbConnection_1 = require("../dbConnection");
const getNews = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield dbConnection_1.pool.query('SELECT * FROM news ORDER BY id DESC');
        return res.status(200).json(response.rows);
    }
    catch (e) {
        console.log(e);
        return res.status(500).json('Server Error to get News');
    }
});
exports.getNews = getNews;
const deleteNews = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = parseInt(req.params.id);
        const response = yield dbConnection_1.pool.query('DELETE FROM news WHERE id = $1', [id]);
        return res.status(200).json(`The item with id ${id} was deleted succesfully`);
    }
    catch (e) {
        console.log(e);
        return res.status(500).json('Server Error to delete item from News');
    }
});
exports.deleteNews = deleteNews;
