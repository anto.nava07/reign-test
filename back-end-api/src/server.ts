import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import compression from 'compression';
import cors from 'cors';
const fetch = require('node-fetch');
import {pool} from './dbConnection';

import indexRoutes from './routes/index';


class Server{
    public app: express.Application;
    
    constructor(){
        this.app=express();
        this.config();
        this.routes();
    }

    config(){
        //Settings
        this.app.set('port',process.env.PORT || 8000);

        //Middleware

        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
        this.app.use(helmet());
        this.app.use(compression()); //reduce el peso de las respuestas
        this.app.use(cors());

    }

    routes(){
        this.app.use(indexRoutes);
    }
    
    start(){
        this.app.listen(this.app.get('port')), () => {
            console.log('Server on port ', this.app.get('port'));
        }
        //Se obtienen los datos de la API al empezar y se guardan en la BDD
        obtenerDatos();
        //Se crea un timeout para volver a obtener los datos luego de una hora
        setTimeout(obtenerDatos, 3600000);
    }

}
//Fin clase Server

const obtenerDatos = async () => {
    const data= await fetch("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
    const body= await data.json();

    const hits= body.hits;

    //Ordena por fecha, de antiguo a reciente para poder guardarse en BDD
    hits.sort((a:any,b:any) => {
        if (a.created_at > b.created_at) {
          return 1;
        }
        if (a.created_at  < b.created_at) {
          return -1;
        }
        return 0;
      });

     guardarEnBDD(hits);


}

const guardarEnBDD = async(hits:any) => {
    //Se guarda cada item de los hits
    hits.map(async (item:any) =>{
        //console.log("Conseguir valor de created_at a partir de un item: ", item.created_at);
        await pool.query('INSERT INTO news (info) VALUES ($1)', [item]);
    });
}


const server = new Server();
server.start();