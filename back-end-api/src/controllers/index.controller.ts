import {Request, Response} from 'express';
import { QueryResult} from 'pg'
import {pool} from '../dbConnection';

export const getNews = async (req: Request, res:Response): Promise<Response> => {
    try{
        const response: QueryResult= await pool.query('SELECT * FROM news ORDER BY id DESC');
        return res.status(200).json(response.rows);
    }catch(e){
        console.log(e);
        return res.status(500).json('Server Error to get News');
    }
}


export const deleteNews = async (req: Request, res:Response): Promise<Response> => {
    try{
        const id= parseInt(req.params.id);
        const response: QueryResult= await pool.query('DELETE FROM news WHERE id = $1', [id]);
        return res.status(200).json(`The item with id ${id} was deleted succesfully`);
    }catch(e){
        console.log(e);
        return res.status(500).json('Server Error to delete item from News');
    }
    

}
