import {Router} from 'express'
const router= Router();
import {getNews,deleteNews} from '../controllers/index.controller' 

router.get('/news',getNews);
router.delete('/news/:id',deleteNews);

export default router;