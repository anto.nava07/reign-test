import React, {useEffect,useState} from 'react'
import Pagination from './Pagination';

const NewsTable = () => {

    //Vars
    const [posts, setPosts]= useState([]);
    const [loading, setLoading]= useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] =useState(5);
    const [q, setQ]=useState("");
    const [search, setSearch]= useState(false);

    //Search by author or title or tags
    function searchFunction(rows){     
        return rows.filter(
            (row)=>
            row.info.author.toLowerCase().indexOf(q.toLowerCase())>-1 ||
            row.info.story_title.toLowerCase().indexOf(q.toLowerCase())>-1 ||
            row.info._tags.find(el => el.toString().toLowerCase().indexOf(q.toLowerCase())>-1 )
        );
  }

    //get data from API and set to posts
    const fetchData = async () => {
        setLoading(true);
        const data= await fetch("http://localhost:8000/news");
        const news= await data.json();
        setPosts(news);
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [])

    //Get current posts
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

    //Change page
    const paginate= (pageNumber ) => setCurrentPage(pageNumber);

    const deleteRow = async(id) => {
        const data= await fetch(`http://localhost:8000/news/${id}`, {
          method: "DELETE",
          
          headers: {"Content-type": "application/json"}
        })
        .then(response => response.json()) 
        .then(json => console.log(json)); 
    
        const filteredArray= posts.filter(el => el.id !== id)
        setPosts(filteredArray);
      }

      //To convert encoded HTML to decoded
      function htmlDecode(str) {
        const doc = new DOMParser().parseFromString(str, "text/html");
        return doc.documentElement.textContent;
    }

      if(loading){
          return <h2>Loading...</h2>
      }

      //If search is pressed once, returns the posts with the typed filter
      if(search){
       return(
        <div>
        <input type="text" placeholder="Filter..." value={q} onChange={(e) => {setQ(e.target.value)}} />
        <button class="btn btn-primary" onClick={() => {setSearch(true)}}>Search</button>
        <p></p>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Title</th>
                <th>Username</th>
                <th>Comment</th>
                <th>Tags</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            {
                    searchFunction(posts).map(item =>(
                        <tr key={item.id}>
                            <td>{item.info.story_title}</td>
                            <td>{item.info.author}</td>
                            <td width="1">{htmlDecode(item.info.comment_text)}</td>
                            <td>{item.info._tags.map(el => {
                                var txt= "#"+ el+" ";
                                return txt;
                                })}</td>
                            <td>
                            <button class="btn btn-danger" onClick={() => {deleteRow(item.id)}}
                            >X</button>
                            </td>
                        </tr>
                    ))
                }
            </tbody>
            </table>
            </div>
       )
      }

      //returns this when refreshing page, since search is false
        return ( 
            <div>
        <input type="text" placeholder="Filter..." value={q} onChange={(e) => {setQ(e.target.value)}} />
        <button class="btn btn-primary" onClick={() => {setSearch(true)}}>Search</button>
        <p></p>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Title</th>
                <th>Username</th>
                <th>Comment</th>
                <th>Tags</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            {
                    currentPosts.map(item =>(
                        <tr key={item.id}>
                            <td>{item.info.story_title}</td>
                            <td>{item.info.author}</td>
                            <td width="1">{htmlDecode(item.info.comment_text)}</td>
                            <td>{item.info._tags.map(el => {
                                var txt= "#"+ el+" ";
                                return txt;
                                })}</td>
                            <td>
                            <button class="btn btn-danger" onClick={() => {deleteRow(item.id)}}
                            >X</button>
                            </td>
                        </tr>
                    ))
                }
            </tbody>
            </table>
            <Pagination 
            postsPerPage={postsPerPage}
            totalPosts={posts.length}
            paginate={paginate}
            />
            </div>
         );
      
    
     
}
 
export default NewsTable; 