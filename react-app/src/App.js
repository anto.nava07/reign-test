import userEvent from '@testing-library/user-event';
import React from 'react'
import NewsTable from './components/NewsTable';

function App() {

  return (
    <div className="container">
      <h1 className="text-primary mb-3">Hacker News</h1>
      <div className="flex-row">
        <div className= "flex-large">
          <h2>Recent posts</h2>
          <NewsTable />
        </div>
      </div>
    </div>
  );
}

export default App;
