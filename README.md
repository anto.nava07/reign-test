# REIGN Test
- The back-end is in the "back-end-api" folder. **Technologies:** Typescript, NodeJS, Express JS, PostgreSQL, etc.
- The API was tested using Postman.
- The front-end is in the "react-app" folder. **Technologies:** React JSX, NodeJS, etc.


## Instructions

### Before starting
- In order to run everything smoothly, you'll need to have installed NodeJS and PostgreSQL to run the commands.
- Note: The following instructions are for Window users.

### Setting up the database

1. In cmd, connect to the PostgreSQL database with user "postgres" and enter the password.
```
psql -U postgres
```
2. Go to "...\back-end-api\database" and open the file named "HackerNews.sql".
3. Create the database copying the first line from "HackerNews.sql" in the cmd.
```
CREATE DATABASE hackernews;
```
4. Connect to the hackernews database.
```
\c hackernews
```
5. Create the table by copying from line 4 to 7 from "HackerNews.sql".
```
CREATE TABLE news (
id serial NOT NULL primary key,
info json NOT NULL
);
```
### Check the connection to the database
1. Go to "...\back-end-api\src" and open the file named "dbConnection.ts"
2. Change the password in line 6 with your current password for the user "postgres" and host, if different.

### Run the API
1. At "...\back-end-api" open cmd and run the following:
```
npm run dev
```
2. After this, you can test the API with Postman or a similar program.
### Start the app
1. At "...\react-app" run the following in cmd.
```
npm start
```
2. Done! Start testing the website. 
